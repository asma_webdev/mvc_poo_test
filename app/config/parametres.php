<?php

/* -----------------------------------------
    PARAMETRES DE CONNEXION A LA DB
   -----------------------------------------
 */
 define('DBHOST', '127.0.0.1');
 define('DBNAME', 'material_blog');
 define('DBUSER', 'root');
 define('DBPWD' , 'root');

 /*
 define('PUBLIC_PATH', 'public');
 define('ADMIN_PATH' , 'backoffice');
*/

 /* -----------------------------------------
     AUTRES PARAMETRES
    -----------------------------------------
  */
  define('DATE_FORMAT', 'D M, Y');

  $zones = ['title', 'content1', 'content2'];
