<?php
/*
./app/modeles/PostsGestionnaire.php
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire extends \Noyau\Classes\GestionnaireGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_modele = '\App\Modeles\Post';
  }

public function findAllByCategorie(int $id ){//id de categories
  $sql="SELECT *
        FROM posts
        JOIN posts_has_categories ON post=posts.id
        WHERE categorie = :id;";
  $rs= App::getConnexion()->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $this->convertPDOStatementToArrayObj($rs);
}

public function findAllByAuteur(int $id){//id de posts
  $sql="SELECT *
        FROM posts
        where auteur = (
              SELECT auteur
              FROM  posts
              where posts.id = :id);";
  $rs= App::getConnexion()->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $this->convertPDOStatementToArrayObj($rs);
}

}
