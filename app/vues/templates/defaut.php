<?php
  use \Noyau\Classes\App;
  use \Noyau\Classes\Template;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MVC POO</title>
  <base href="<?php echo \Noyau\Classes\App::getRoot(); ?>" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<body class="container">
  <h1><a href="<?php echo \Noyau\Classes\App::getRoot(); ?>">MVC POO</a></h1>
  <main class="row">
    <div class="col m8">
      <?php echo Template::getZone('content1'); ?>

    </div>
    <aside class="col m4">
        <?php if (isset($_GET['posts'])) {
           echo Template::getZone('content2');
        }


         ?>
        <?php
        $ctrl = new \App\Controleurs\CategoriesControleur();
        $ctrl->indexAction([
          'orderBy' => 'titre'
        ]);
       ?>
       <div class="">

         <?php
           $ctrl = new \App\Controleurs\PostsControleur();
           $ctrl->indexAction([
             'orderBy' => 'datePublication',
             'orderSens' => 'DESC',
             'limit' =>2,
           ], 'latest');
          ?>
       </div>
    </aside>
  </main>

</body>
</html>
