<?php
/*
./app/vues/categories/show.php
Variables disponibles :
    - $categorie Categorie
 */
 use \Noyau\Classes\Template;
 ?>
 <?php Template::startZone('title'); ?>
  <?php echo $categorie->getTitre(); ?>
 <?php Template::stopZone(); ?>

<?php Template::startZone('content1'); ?>
  <h2><?php echo $categorie->getTitre(); ?></h2>
  <?php
   $ctrl = new \App\Controleurs\PostsControleur();
   $ctrl->indexByCategorieAction($categorie->getId());
   ?>
<?php Template::stopZone(); ?>
