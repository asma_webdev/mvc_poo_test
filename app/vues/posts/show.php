<?php
/*
./app/vues/posts/show.php
Variables disponibles :
    - $post Post
 */
 use \Noyau\Classes\Template;
 ?>
 <?php Template::startZone('title'); ?>
  <?php echo $post->getTitre(); ?>
 <?php Template::stopZone(); ?>

<?php Template::startZone('content1'); ?>
<article class="">
  <header>
    <h1><?php echo $post->getTitre(); ?></h1>
    <div>Auteur: <?php echo $post->getAuteur(); ?> -
      <time datetime="<?php echo \Noyau\Classes\App::datify($post->getDatePublication(), 'Y-m-d'); ?>">
        <?php echo \Noyau\Classes\App::datify($post->getDatePublication()); ?>
      </time>
    </div>
  </header>
  <div class="">
    <?php echo $post->getTexte(); ?>
  </div>
</article>

<?php Template::stopZone(); ?>
