<?php
/*
./app/vues/posts/liste.php
Variables disponibles :
    - $posts ARRAY(Post)
 */
 use \Noyau\Classes\Template;
?>

 <?php foreach ($posts as $post): ?>

     <li>
       <a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>">
         <?php echo $post->getTitre(); ?>
       </a>
     </li>

 <?php endforeach; ?>
