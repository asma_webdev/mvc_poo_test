<?php
/*
./app/controleurs/PostsControleur.php
 */

namespace App\Controleurs;

class PostsControleur extends \Noyau\Classes\ControleurGenerique {

  public function __construct(){
    $this->_table = 'posts';
    parent::__construct();
  }

  public function indexByCategorieAction(int $id){
    $posts = $this->_gestionnaire->findAllByCategorie($id);
    include '../app/vues/posts/liste.php';
  }

  public function indexByAuteurAction(int $id){
    $posts = $this->_gestionnaire->findAllByAuteur($id);
    include '../app/vues/posts/listAuteur.php';
  }

}
